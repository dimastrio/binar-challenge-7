package id.dimas.challenge7

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import id.dimas.challenge7.adapter.MovieAdapter
import id.dimas.challenge7.databinding.FragmentHomeBinding
import id.dimas.challenge7.helper.DataStoreManager
import id.dimas.challenge7.helper.viewModelsFactory
import id.dimas.challenge7.model.Status
import id.dimas.challenge7.repo.MovieRepo
import id.dimas.challenge7.repo.UserRepo
import id.dimas.challenge7.service.TMDBApiService
import id.dimas.challenge7.service.TMDBClient
import id.dimas.challenge7.viewmodel.HomeViewModel


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val pref: DataStoreManager by lazy { DataStoreManager(requireContext()) }
    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val movieRepo: MovieRepo by lazy { MovieRepo(apiService) }
    private val viewModel: HomeViewModel by viewModelsFactory { HomeViewModel(pref, movieRepo) }

    private lateinit var movieAdapter: MovieAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeLogo()
        observeData()
        observeMovie()
        initRecyclerView()
        moveProfile()
    }

    private fun changeLogo() {
        if (BuildConfig.FLAVOR == "staging") {
            binding.ivLogo.setImageResource(R.drawable.finash_logo)
        } else {
            binding.ivLogo.setImageResource(R.drawable.tmdb_logo)
        }
    }

    private fun moveProfile() {
        binding.apply {
            ivProfile.setOnClickListener {
                it.findNavController()
                    .navigate(HomeFragmentDirections.actionHomeFragmentToProfileFragment())
            }
        }
    }

    private fun initRecyclerView() {
        movieAdapter = MovieAdapter { movie_id ->
            viewModel.action(movie_id)
        }
        binding.rvMovie.apply {
            adapter = movieAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    private fun observeData() {
        viewModel.getUsername().observe(viewLifecycleOwner) {
            val username = it
            binding.tvUsername.text = "Welcome, $username"
        }

//        viewModel.user.observe(viewLifecycleOwner){ username ->
//            binding.tvUsername.text = username
//        }


    }

    private fun observeMovie() {
        viewModel.movieBundle.observe(viewLifecycleOwner) {
            findNavController().navigate(R.id.action_homeFragment_to_detailMovieFragment, it)
        }

        viewModel.getAllMovie("072af9268186d5a57f828c9f3dd51aac").observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.pb.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.pb.isVisible = false
                    it.data?.let { response ->
                        movieAdapter.updateData(response.movieItems)
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    binding.pb.isVisible = false
                }
            }
        }
    }

    private fun messageToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }


}