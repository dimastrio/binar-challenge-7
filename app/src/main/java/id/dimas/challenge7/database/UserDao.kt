package id.dimas.challenge7.database

import androidx.room.*

@Dao
interface UserDao {

    @Query("SELECT * FROM User WHERE email =:email")
    fun checkEmailUser(email: String): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User): Int

    @Query("SELECT username FROM User WHERE email =:email")
    fun getUsername(email: String): String

}