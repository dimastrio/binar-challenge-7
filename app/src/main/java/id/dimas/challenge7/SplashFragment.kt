package id.dimas.challenge7

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.dimas.challenge7.databinding.FragmentSplashBinding
import id.dimas.challenge7.viewmodel.SplashViewModel
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel


class SplashFragment : Fragment() {

    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SplashViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeLogo()
        lifecycleScope.launchWhenCreated {
            delay(3000)
            checkLogin()
        }
    }

    private fun changeLogo() {
        if (BuildConfig.FLAVOR == "staging") {
            binding.ivLogo.setImageResource(R.drawable.finash_logo)
        } else {
            binding.ivLogo.setImageResource(R.drawable.tmdb_logo)
        }
    }

    private fun checkLogin() {
        viewModel.getLogin().observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
            } else {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment())
            }
        }
    }


}