package id.dimas.challenge7

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.dimas.challenge7.database.User
import id.dimas.challenge7.databinding.FragmentProfileBinding
import id.dimas.challenge7.helper.DataStoreManager
import id.dimas.challenge7.repo.UserRepo
import id.dimas.challenge7.viewmodel.ProfileViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private val pref: DataStoreManager by lazy { DataStoreManager(requireContext()) }
    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val viewModel: ProfileViewModel by viewModel()

    private var userId: Int = 0
    private var email: String = ""
    private var password: String = ""
    private var image: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeLogo()
        observeData()
        setProfile()
        logout()
        setImage()
    }

    private fun changeLogo() {
        if (BuildConfig.FLAVOR == "staging") {
            binding.ivLogo.setImageResource(R.drawable.finash_logo)
        } else {
            binding.ivLogo.setImageResource(R.drawable.tmdb_logo)
        }
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            viewModel.logout()
            messageToast("Logout")
            it.findNavController()
                .navigate(ProfileFragmentDirections.actionProfileFragmentToLoginFragment())
        }
    }


    private fun setProfile() {
        binding.apply {
            btnUpdate.setOnClickListener {
                val username = etUsername.text.toString()
                val fullname = etFullname.text.toString()
                val datebirth = etDatebirth.text.toString()
                val address = etAddress.text.toString()
                when {
                    username.isEmpty() -> {
                        etUsername.error = "Username tidak Boleh Kosong"
                    }
                    fullname.isEmpty() -> {
                        etFullname.error = "Nama lengkap tidak boleh kosong"
                    }
                    datebirth.isEmpty() -> {
                        etDatebirth.error = "Tanggal lahir tidak boleh kosong"
                    }
                    address.isEmpty() -> {
                        etAddress.error = "Alamat tidak boleh kosong"
                    }
                    else -> {
                        val newUser = User(
                            userId,
                            username,
                            email,
                            password,
                            fullname,
                            datebirth,
                            address
                        )
                        viewModel.updateToDb(newUser)
                    }
                }
            }

        }
    }

    private fun setImage() {
        binding.ivLogo.setOnClickListener {
            checkPermission()
        }
    }


    private fun observeData() {

        viewModel.updateMessage.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToHomeFragment())
        }

        viewModel.getEmail().observe(viewLifecycleOwner) {
            viewModel.getData(it)
        }

        viewModel.user.observe(viewLifecycleOwner) {
            if (it != null) {
                binding.apply {
                    etUsername.setText(it.username)
                    etFullname.setText(it.fullname)
                    etDatebirth.setText(it.datebirth)
                    etAddress.setText(it.address)
                }
            }
            userId = it.userId!!.toInt()
            email = it.email
            password = it.password
        }
    }

    private fun checkPermission() {
        // apakah permission sudah di setujui atau belum
        if (isGranted(
                requireActivity(),
                Manifest.permission.CAMERA,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_CODE_PERMISSION,
            )
        ) {
            chooseImageDialog()
        }
    }

    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int,
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // klau udah di tolak sebelumnya
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            }
            // klau belum pernah di tolak (request pertama kali)
            else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    // dialoag yg muncul kalau user menolak permission yg di butuhkan
    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton(
                "App Settings"
            ) { _, _ ->
                // mengarahkan user untuk buka halaman setting
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", requireActivity().packageName, null)
                intent.data = uri
                startActivity(intent)
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            .show()
    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(requireContext())
            .setMessage("Pilih Gambar")
            .setPositiveButton("Gallery") { _, _ -> openGallery() }
            .setNegativeButton("Camera") { _, _ -> openCamera() }
            .show()
    }

    // buat dapetin URI image gallery
    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
            // munculin image dari gallery ke ImageView
            binding.ivLogo.setImageURI(result)
        }

    // buat buka gallery
    private fun openGallery() {
        requireActivity().intent.type = "image/*"
        galleryResult.launch("image/*")
    }

    // buat dapeting bitmap image dari camera
    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK && result.data != null) {
                // dapetin data bitmap dari intent
                val bitmap = result.data!!.extras?.get("data") as Bitmap

                // load bitmap ke dalam imageView
                binding.ivLogo.setImageBitmap(bitmap)
            }
        }

    // buat open camera
    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraResult.launch(cameraIntent)
    }

    private fun messageToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }


    companion object {
        const val REQUEST_CODE_PERMISSION = 100
    }
}