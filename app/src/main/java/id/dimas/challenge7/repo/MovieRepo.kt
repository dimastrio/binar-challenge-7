package id.dimas.challenge7.repo

import id.dimas.challenge7.service.TMDBApiService

class MovieRepo(private val apiService: TMDBApiService) {

    suspend fun getMovie(key: String) = apiService.getAllMovie(key)

    suspend fun getDetailMovie(movie_id: Int, key: String) =
        apiService.getDetailMovie(movie_id, key)
}