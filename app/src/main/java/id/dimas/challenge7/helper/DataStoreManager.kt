package id.dimas.challenge7.helper

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DataStoreManager(private val context: Context) {

    fun prefLogin(): Flow<Boolean> {
        return context.dataStore.data.map { pref ->
            pref[IS_LOGIN] ?: false
        }
    }

    fun getUsername(): Flow<String> {
        return context.dataStore.data.map { pref ->
            pref[KEY_USERNAME] ?: "Guys!"
        }
    }

    fun getEmail(): Flow<String> {
        return context.dataStore.data.map { pref ->
            pref[KEY_EMAIL] ?: ""
        }
    }

//    fun getDataUser(): Flow<Any> {
//        return context.dataStore.data.map { pref ->
//            pref[KEY_ID] = userId
//            pref[KEY_USERNAME] = username
//            pref[KEY_EMAIL] = email
//            pref[KEY_PASS] = password
//            pref[IS_LOGIN] = true
//        }
//    }

//    fun getUserId(): Flow<Int> {
//        return context.dataStore.data.map { pref ->
//            pref[KEY_ID] ?: 0
//        }
//    }
//
//    fun getPassword(): Flow<String> {
//        return context.dataStore.data.map { pref ->
//            pref[KEY_PASS] ?: ""
//        }
//    }
//
//    fun getMovieId(): Flow<Int> {
//        return context.dataStore.data.map { pref ->
//            pref[KEY_MOVIE_ID] ?: 0
//        }
//    }

    suspend fun setData(
        userId: Int,
        username: String,
        email: String,
        password: String
    ) {
        context.dataStore.edit { pref ->
            pref[KEY_ID] = userId
            pref[KEY_USERNAME] = username
            pref[KEY_EMAIL] = email
            pref[KEY_PASS] = password
            pref[IS_LOGIN] = true
        }
    }

    suspend fun cleanPref() {
        context.dataStore.edit { pref ->
            pref.clear()
        }
    }


    companion object {

        private const val DATA_STORE_NAME = "counter_pref"

        private val KEY_ID = intPreferencesKey("id")
        private val KEY_USERNAME = stringPreferencesKey("username")
        private val KEY_PASS = stringPreferencesKey("password")
        private val KEY_EMAIL = stringPreferencesKey("email")
        private val IS_LOGIN = booleanPreferencesKey("isLogin")
        private val KEY_MOVIE_ID = intPreferencesKey("movie_id")

        private val Context.dataStore by preferencesDataStore(name = DATA_STORE_NAME)
    }
}