package id.dimas.challenge7

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import id.dimas.challenge7.databinding.FragmentDetailMovieBinding
import id.dimas.challenge7.helper.DataStoreManager
import id.dimas.challenge7.helper.toDate
import id.dimas.challenge7.model.Status
import id.dimas.challenge7.repo.MovieRepo
import id.dimas.challenge7.service.TMDBApiService
import id.dimas.challenge7.service.TMDBClient
import id.dimas.challenge7.viewmodel.DetailMovieViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class DetailMovieFragment : Fragment() {

    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!

    private val pref: DataStoreManager by lazy { DataStoreManager(requireContext()) }
    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val movieRepo: MovieRepo by lazy { MovieRepo(apiService) }
    private val viewModel: DetailMovieViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeMovie()
        moveToHome()
    }


    private fun moveToHome() {
        binding.ivBack.setOnClickListener {
            it.findNavController()
                .navigate(DetailMovieFragmentDirections.actionDetailMovieFragmentToHomeFragment())
        }
    }

    private fun observeMovie() {
        val movieId = arguments?.getInt("movie_id")

        viewModel.getDetailMovie(movieId ?: 0).observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.pb.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.pb.isVisible = false
                    it.data?.let { response ->
                        binding.apply {
                            Glide.with(requireContext())
                                .load("https://image.tmdb.org/t/p/w500${it.data.backdropPath}")
                                .into(ivBackdrop)
                            Glide.with(requireContext())
                                .load("https://image.tmdb.org/t/p/w500${it.data.posterPath}")
                                .into(ivMovie)
                            tvTitleMovie.text = it.data.title
                            tvReleaseDate.text = it.data.releaseDate.toDate()
                            tvOverview.text = it.data.overview
                        }
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    binding.pb.isVisible = false
                }
            }
        }
    }

    private fun messageToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }


}