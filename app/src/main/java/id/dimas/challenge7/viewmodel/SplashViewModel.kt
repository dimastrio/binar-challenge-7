package id.dimas.challenge7.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.dimas.challenge7.helper.DataStoreManager

class SplashViewModel(private val pref: DataStoreManager) : ViewModel() {

    fun getLogin(): LiveData<Boolean> {
        return pref.prefLogin().asLiveData()
    }
}