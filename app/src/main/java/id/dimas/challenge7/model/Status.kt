package id.dimas.challenge7.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
