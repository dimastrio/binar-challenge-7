package id.dimas.challenge7

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import id.dimas.challenge7.databinding.FragmentRegisterBinding
import id.dimas.challenge7.repo.UserRepo
import id.dimas.challenge7.viewmodel.RegisterViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val viewModel: RegisterViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeLogo()
        addUser()
        observeData()
    }

    private fun changeLogo() {
        if (BuildConfig.FLAVOR == "staging") {
            binding.ivLogo.setImageResource(R.drawable.finash_logo)
        } else {
            binding.ivLogo.setImageResource(R.drawable.tmdb_logo)
        }
    }

    private fun addUser() {
        binding.apply {
            btnRegister.setOnClickListener {
                val email = etEmail.text.toString()
                val username = etUsername.text.toString()
                val password = etPassword.text.toString()
                val confPass = etConfPass.text.toString()
                if (username.isEmpty()) {
                    etUsername.error = "Username tidak boleh kosong"
                } else if (email.isEmpty()) {
                    etEmail.error = "Email tidak boleh kosong"
                } else if (password.isEmpty()) {
                    etPassword.error = "Password tidak boleh kosong"
                } else if (confPass.isEmpty()) {
                    etConfPass.error = "Konfirmasi Password harus diisi"
                } else {
                    if (password == confPass) {
                        viewModel.saveUserToDb(username, email, password, "", "", "")
                    } else {
                        etConfPass.error = "Konfirmasi Password Anda Tidak Sesuai"
                    }
                }
            }
        }
    }

    private fun observeData() {
        viewModel.successMessage.observe(viewLifecycleOwner) {
            messageToast(it)
            findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment())
        }

        viewModel.failedMessage.observe(viewLifecycleOwner) {
            messageToast(it)
        }
    }

    private fun messageToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }
}