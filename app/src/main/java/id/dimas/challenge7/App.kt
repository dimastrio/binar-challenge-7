package id.dimas.challenge7

import android.app.Application
import id.dimas.challenge7.helper.DataStoreManager
import id.dimas.challenge7.repo.MovieRepo
import id.dimas.challenge7.repo.UserRepo
import id.dimas.challenge7.service.TMDBClient
import id.dimas.challenge7.viewmodel.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    val appModule = module {
        single { DataStoreManager(androidContext()) }
        single { TMDBClient.instance }
        single { MovieRepo(get()) }
        single { UserRepo(androidContext()) }
    }

    val viewModelModule = module {
        single { DetailMovieViewModel(get(), get()) }
        single { LoginViewModel(get(), get()) }
        single { HomeViewModel(get(), get()) }
        single { RegisterViewModel(get()) }
        single { ProfileViewModel(get(), get()) }
        single { SplashViewModel(get()) }
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, viewModelModule))
        }
    }
}